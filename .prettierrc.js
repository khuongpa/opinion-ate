module.exports = {
  arrowParens: "always",
  bracketSpacing: true,
  singleQuote: false,
  trailingComma: "es5",
};
